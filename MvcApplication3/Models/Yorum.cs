using System;
using System.Collections.Generic;

namespace MvcApplication3.Models
{
    public partial class Yorum
    {
        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public int EtkinlikID { get; set; }
        public System.DateTime EklenmeTarihi { get; set; }
        public System.Guid YazarId { get; set; }
        public bool Aktif { get; set; }
        public virtual Etkinlik Etkinlik { get; set; }
        public virtual Kullanici Kullanici { get; set; }
    }
}
