using System;
using System.Collections.Generic;

namespace MvcApplication3.Models
{
    public partial class Kategori
    {
        public Kategori()
        {
            this.Etkinliks = new List<Etkinlik>();
            this.SiteTakips = new List<SiteTakip>();
        }

        public int Id { get; set; }
        public string Adi { get; set; }
        public virtual ICollection<Etkinlik> Etkinliks { get; set; }
        public virtual ICollection<SiteTakip> SiteTakips { get; set; }
    }
}
