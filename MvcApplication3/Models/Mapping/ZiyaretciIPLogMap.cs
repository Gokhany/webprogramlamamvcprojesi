using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MvcApplication3.Models.Mapping
{
    public class ZiyaretciIPLogMap : EntityTypeConfiguration<ZiyaretciIPLog>
    {
        public ZiyaretciIPLogMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IpAddress, t.Tarih });

            // Properties
            this.Property(t => t.IpAddress)
                .IsRequired()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("ZiyaretciIPLog");
            this.Property(t => t.IpAddress).HasColumnName("IpAddress");
            this.Property(t => t.Tarih).HasColumnName("Tarih");
        }
    }
}
