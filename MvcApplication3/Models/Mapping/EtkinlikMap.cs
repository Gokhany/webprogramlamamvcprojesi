using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MvcApplication3.Models.Mapping
{
    public class EtkinlikMap : EntityTypeConfiguration<Etkinlik>
    {
        public EtkinlikMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Baslik)
                .IsRequired()
                .HasMaxLength(140);

            this.Property(t => t.Icerik)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Etkinlik");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Baslik).HasColumnName("Baslik");
            this.Property(t => t.Icerik).HasColumnName("Icerik");
            this.Property(t => t.YayimTarih).HasColumnName("YayimTarih");
            this.Property(t => t.KategoriID).HasColumnName("KategoriID");
            this.Property(t => t.EtkinliTipID).HasColumnName("EtkinliTipID");
            this.Property(t => t.YazarID).HasColumnName("YazarID");
            this.Property(t => t.KapakResimID).HasColumnName("KapakResimID");
            this.Property(t => t.Goruntulenme).HasColumnName("Goruntulenme");
            this.Property(t => t.Begeni).HasColumnName("Begeni");
            this.Property(t => t.Aktig).HasColumnName("Aktig");

            // Relationships
            this.HasMany(t => t.Resims)
                .WithMany(t => t.Etkinliks1)
                .Map(m =>
                    {
                        m.ToTable("EtkinlikResim");
                        m.MapLeftKey("EtkinlikID");
                        m.MapRightKey("ResimID");
                    });

            this.HasRequired(t => t.EtkinlikTip)
                .WithMany(t => t.Etkinliks)
                .HasForeignKey(d => d.EtkinliTipID);
            this.HasRequired(t => t.Kategori)
                .WithMany(t => t.Etkinliks)
                .HasForeignKey(d => d.KategoriID);
            this.HasRequired(t => t.Resim)
                .WithMany(t => t.Etkinliks)
                .HasForeignKey(d => d.KapakResimID);
            this.HasRequired(t => t.Kullanici)
                .WithMany(t => t.Etkinliks)
                .HasForeignKey(d => d.YazarID);

        }
    }
}
