using System;
using System.Collections.Generic;

namespace MvcApplication3.Models
{
    public partial class Etiket
    {
        public Etiket()
        {
            this.Etkinliks = new List<Etkinlik>();
        }

        public int Id { get; set; }
        public string Adi { get; set; }
        public virtual ICollection<Etkinlik> Etkinliks { get; set; }
    }
}
