using System;
using System.Collections.Generic;

namespace MvcApplication3.Models
{
    public partial class ZiyaretciIPLog
    {
        public string IpAddress { get; set; }
        public System.DateTime Tarih { get; set; }
    }
}
