﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcApplication3.Controllers
{
    public class KullaniciController : Controller
    {
        //
        // GET: /Kullanici/
        etkinlikContext etkinlik = new etkinlikContext();
        public ActionResult GirisYap()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GirisYap(string kullaniciAdi,string Parola)
        {
            if (System.Web.Security.Membership.ValidateUser(kullaniciAdi, Parola))
            {
                FormsAuthentication.RedirectFromLoginPage(kullaniciAdi, true);
                Guid id = (Guid)System.Web.Security.Membership.GetUser(kullaniciAdi).ProviderUserKey;
                Session["Kullanici"]=etkinlik.Kullanicis.FirstOrDefault(x=>x.Id==id);
                
                return RedirectToAction("Index", "Yonetim");
            }
            else {

                ViewBag.Mesaj = "Kullanıcı Adı veya Parola Yanlış";
                    return View();
            
            }
            
        }
        public ActionResult KayitOl()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KayitOl(Kullanici kullanici,HttpPostedFileBase Resim,string Parola)
        {
            try
            {
                MembershipUser user = System.Web.Security.Membership.CreateUser(kullanici.Nick, Parola, kullanici.Mail);
                kullanici.Id = (Guid)user.ProviderUserKey;
                Session["Kullanici"] = kullanici;
                kullanici.ResimID = YonetimController.ResimKaydet(Resim, HttpContext);
                kullanici.KayitTarihi = DateTime.Now;
                etkinlik.Kullanicis.Add(kullanici);
                etkinlik.SaveChanges();
                FormsAuthentication.RedirectFromLoginPage(kullanici.Nick, true);
                Session["Kullanici"] = kullanici;
            }
            catch (DbEntityValidationException ex)
            {
                throw;
            }
            return RedirectToAction("Index","Home");
        }
    }
}
