﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication3.Controllers
{
    public class YazarController : Controller
    {
        //
        // GET: /Yazar/
        etkinlikContext etkinlik = new etkinlikContext();
        public ActionResult Index(Guid id)
        {
            return View(id);
        }
        public ActionResult EtkinlikListele(Guid id)
        {
            var data = etkinlik.Etkinliks.Where(x=>x.YazarID ==id);
            return View("EtkinlikListele",data);
        }
    }
}
