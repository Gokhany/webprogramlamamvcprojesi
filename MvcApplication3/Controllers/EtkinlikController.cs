﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication3.Controllers
{
    public class EtkinlikController : Controller
    {
        //
        // GET: /Etkinlik/
        etkinlikContext etkinlik = new etkinlikContext();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult TariheGoreListe(int yil,int ay)
        {
            ViewBag.yil = yil;
            ViewBag.ay = ay;
            return View();
        }
        public ActionResult EtkinlikListele(int yil=0,int ay=0)
        {
            var data = etkinlik.Etkinliks.Where(x => x.YayimTarih.Year == yil && x.YayimTarih.Month==ay);
            return View("EtkinlikListele", data);
        }
        public ActionResult Detay(int id)
        {
            
            ViewBag.Kullaici = Session["Kullanici"];

            Etkinlik etk = etkinlik.Etkinliks.FirstOrDefault(x=>x.Id == id);
            etk.Goruntulenme++;
            etkinlik.SaveChanges();
            return View(etk);
        }

        [HttpPost]
        public ActionResult YorumYaz(Yorum yorum)
        {
            yorum.EklenmeTarihi = DateTime.Now;
            yorum.Baslik = "";
            yorum.Aktif = false;    
            etkinlik.Yorums.Add(yorum);
            etkinlik.SaveChanges();
            return RedirectToAction("Detay", new {id=yorum.EtkinlikID });
        }
        public string Begen(int id)
        {
            Etkinlik ekn = etkinlik.Etkinliks.FirstOrDefault(x=>x.Id==id);
            ekn.Begeni++;
            etkinlik.SaveChanges();
            return ekn.Begeni.ToString();
        }
        public void Goruntulendi(int id)
        {
            Etkinlik ekn = etkinlik.Etkinliks.FirstOrDefault(x => x.Id == id);
            ekn.Goruntulenme++;
            etkinlik.SaveChanges();
            
        
        }
    }
}
