﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication3.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        etkinlikContext etkinlik = new etkinlikContext();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CategoryWidgetGetir()
        {
            var kat = etkinlik.Kategoris.ToList();
            return View(etkinlik.Kategoris);
        }
        public ActionResult PostsWidgetGetir()
        {
            ViewBag.Fresh = etkinlik.Etkinliks.OrderByDescending(x => x.YayimTarih).Take(5);
            ViewBag.Populer = etkinlik.Etkinliks.OrderByDescending(x => x.Goruntulenme).Take(5);
            return View();
        }
        public ActionResult TagsWidgetGetir()
        {
            var tags = etkinlik.Etikets.ToList();
            return View(tags);
        }
        public ActionResult TumEtkinliklerGetir()
        {
            var etkinlikler = etkinlik.Etkinliks.ToList();
            return View("EtkinlikListele",etkinlikler);
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }  
    }
}
