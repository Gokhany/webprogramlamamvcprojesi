﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication3.Controllers
{
    public class EtiketController : Controller
    {
        //
        // GET: /Etiket/
        etkinlikContext etkinlik = new etkinlikContext();
        public ActionResult Index(int id)
        {
            return View();
        }
        public ActionResult EtkinlikListele(int id)
        {
            var data = etkinlik.Etkinliks.Where(x=>x.Etikets.Any(ee=>ee.Id==id));
            return View("EtkinlikListele",data);
        }
    }
}
