﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication3.Controllers
{
    using Models;
    [Authorize(Roles="Admin")] //Admin yetkilendirme..
    
    public class YonetimController : Controller
    {
        //
        // GET: /Yonetim/
        etkinlikContext etkinlik = new etkinlikContext();
        public ActionResult Index()
        {
            ViewBag.tip = 1;
            return View(etkinlik.Etkinliks.ToList());
        }
        public ActionResult EtkinlikYaz()
        {
            ViewBag.tip = 1;
            ViewBag.Kategoriler = etkinlik.Kategoris.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult EtkinlikYaz(Etkinlik etk1,HttpPostedFileBase Resim,string etiketler)
        {
            if (etk1 != null)
            {
                Kullanici aktif=Session["Kullanici"] as Kullanici;
                etk1.YayimTarih = DateTime.Now;
                etk1.EtkinliTipID = 1;
                etk1.YazarID = aktif.Id;
                etk1.KapakResimID = ResimKaydet(Resim,HttpContext);
                etkinlik.Etkinliks.Add(etk1);
                etkinlik.SaveChanges();

                string[] etikets = etiketler.Split(',');
                foreach (string etiket in etikets)
                {
                    Etiket etk = etkinlik.Etikets.FirstOrDefault(x => x.Adi.ToLower() == etiket.ToLower().Trim());
                    if (etk == null)
                    {
                        etk=new Etiket();
                        etk.Adi = etiket;
                        etkinlik.Etikets.Add(etk);
                        etkinlik.SaveChanges();
     
                    }
                        etk1.Etikets.Add(etk);
                        etkinlik.SaveChanges();
                   
                }

            }
            return RedirectToAction("Index");
        }

        public static int ResimKaydet(HttpPostedFileBase Resim,HttpContextBase ctx)
        {
            etkinlikContext etkinlik = new etkinlikContext();
            
            int kucukWidth = Convert.ToInt32(ConfigurationManager.AppSettings["kw"]);
            int kucukHeight = Convert.ToInt32(ConfigurationManager.AppSettings["kh"]);
            int ortaWidth = Convert.ToInt32(ConfigurationManager.AppSettings["ow"]);
            int ortaHeight = Convert.ToInt32(ConfigurationManager.AppSettings["oh"]);
            int buyukWidth = Convert.ToInt32(ConfigurationManager.AppSettings["bw"]);
            int buyukHeight = Convert.ToInt32(ConfigurationManager.AppSettings["bh"]);

            string newName = Path.GetFileNameWithoutExtension(Resim.FileName) + Guid.NewGuid() + Path.GetExtension(Resim.FileName);

            Image orjRes = Image.FromStream(Resim.InputStream);
            Bitmap kucukRes = new Bitmap(orjRes, kucukWidth, kucukHeight);
            Bitmap ortaRes = new Bitmap(orjRes, ortaWidth, ortaHeight);
            Bitmap buyukRes = new Bitmap(orjRes);

            kucukRes.Save(ctx.Server.MapPath("~/Content/Resimler/Kucuk/"+newName));
            ortaRes.Save(ctx.Server.MapPath("~/Content/Resimler/Orta/" + newName));
            buyukRes.Save(ctx.Server.MapPath("~/Content/Resimler/Buyuk/" + newName));

            Kullanici k = (Kullanici)ctx.Session["Kullanici"];

            Resim dbRes = new Resim();
            dbRes.Adi = Resim.FileName;
            dbRes.BuyukResimYol = "/Content/Resimler/Buyuk/" + newName;
            dbRes.OrtaResimYol = "/Content/Resimler/Orta/" + newName;
            dbRes.KucukResimYol = "/Content/Resimler/Kucuk/" + newName;

            dbRes.EklenmeTarihi = DateTime.Now;
            dbRes.EkleyenID = k.Id;

            etkinlik.Resims.Add(dbRes);
            etkinlik.SaveChanges();

            return dbRes.Id;
        }

        public ActionResult Kategori()
        {
            ViewBag.Tip = 1;
            return View(etkinlik.Kategoris.ToList());
        }
        public ActionResult KategoriEkle()
        {
            ViewBag.Tip = 1;
            return View();
        }
        [HttpPost]
        public ActionResult KategoriEkle(Kategori kat)
        {
            etkinlik.Kategoris.Add(kat);
            etkinlik.SaveChanges();
            return RedirectToAction("Kategori");
        }
        public ActionResult KategoriDuzenle(int id)
        {
            ViewBag.Tip = 1;
            return View(etkinlik.Kategoris.FirstOrDefault(x=>x.Id == id));
        }
        [HttpPost]
        public ActionResult KategoriDuzenle(Kategori kat)
        {
            etkinlik.Entry(kat).State = System.Data.EntityState.Modified;
            etkinlik.SaveChanges();
            return RedirectToAction("Kategori");
        }
        public ActionResult KategoriSil(int id)
        {
            etkinlik.Kategoris.Remove(etkinlik.Kategoris.FirstOrDefault(x=>x.Id==id));
            etkinlik.SaveChanges();
            return RedirectToAction("Kategori");

        }
        [Authorize(Roles="Admin")]
        public ActionResult Rol()
        {

            return View();
        }
    }
}
