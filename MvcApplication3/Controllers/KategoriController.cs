﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication3.Controllers
{
    public class KategoriController : Controller
    {
        //
        // GET: /Kategori/

        etkinlikContext etkinlik = new etkinlikContext();

        public ActionResult Index(int id)
        {
        
            return View(id);
        }
        public ActionResult Etkinlistele(int id)
        {
            var data = etkinlik.Etkinliks.Where(x => x.KategoriID == id);
            return View("EtkinlikListele",data);
        }
    }
}
